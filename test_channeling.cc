#include <iostream>
#include <list>

#include "Fot.h"
#include "Evenement.h"
#include "RunParameters.h"
#include "Crystal.h"
#include "PhotonCollection.h"

#include "TRandom.h"
#include "TFile.h"
#include "TTree.h"

int main(int argc, char **argv){

  double xtal_thick = 1.4e7; // angstrom 
  double sigmaXY = 2.5e7; // angstrom 
  double deltaE = 1.0e-3;
  double Energy = 5; // GeV
  double sigmaPxy = 1.0e-5;
  double x0 = 0, y0 =0;
  double px0 = 0, py0 = 0;


  Crystal crysW("W",111);
  RunParameters rp(crysW);

  double phomin = 2.0e-3;
  double etmax = 100000.0;
  double vtmax = 1e-2;
  double poimin = 1.0;
  double frekuma = 3.0/MC_OVER_HBAR;

  rp.setZexit(xtal_thick);
  rp.set( phomin, etmax, vtmax,poimin, frekuma);

  ParticleCollection partColl;
  double charge = -1.0;

  // For electrons
  double gamma = Energy/0.511;
  double x=0, y=0;
  double primary_energy = 5, px=0, py=0;

  // For gamma
  double x_gamma = 0;
  double y_gamma = 0;
  double z_gamma = 0;
  double px_gamma = 0;
  double py_gamma = 0;
  double pz_gamma = 0;
  double e_gamma = 0;


  TFile * fileROOT  = new TFile("output_Fot.root", "RECREATE");
  TTree tree("tree","A tree with particle information");

  tree.Branch("x",&x_gamma,"x/D");
  tree.Branch("y",&y_gamma,"y/D");
  tree.Branch("z",&z_gamma,"z/D");
  tree.Branch("px",&px_gamma,"px/D");
  tree.Branch("py",&py_gamma,"py/D");
  tree.Branch("pz",&pz_gamma,"pz/D");
  tree.Branch("e",&e_gamma,"e/D");


  int N_events = 10000;

  for (int i = 0;i<N_events;i++){

    x = gRandom->Gaus(x0,sigmaXY);
    y = gRandom->Gaus(y0,sigmaXY);
    primary_energy = gRandom->Gaus(Energy,Energy * deltaE);
    gamma = primary_energy/ELECTRON_MASS_GEV;
    px = gRandom->Gaus(px0,sigmaPxy)*primary_energy;
    py = gRandom->Gaus(py0,sigmaPxy)*primary_energy;

    Fot *pFot = new Fot(rp);
    Particle* part = new Particle(charge,x,y,0,px/ELECTRON_MASS_GEV,py/ELECTRON_MASS_GEV,gamma);
    pFot->makeSingleParticleKumakhov(part);
    delete part;


    list< struct Photon >::iterator it;
    list<Photon> list_photon_kumakov = pFot->getPhotonCollection().getPhotonList();
    delete pFot;

    for(it = list_photon_kumakov.begin() ; it != list_photon_kumakov.end() ; it++){
      e_gamma = it->getEnergy();
      px_gamma = it->getThetax()*e_gamma;
      py_gamma = it->getThetay()*e_gamma;
      pz_gamma = sqrt(pow(e_gamma,2) - pow(px_gamma,2) -pow(py_gamma,2));
      x_gamma = it->getXemis() +x; // angstrom 
      y_gamma = it->getYemis() +y; // angstrom
      z_gamma = it->getZemis(); // angstrom

      tree.Fill();

    }
  }

  tree.Write();

  fileROOT->Close();
  delete fileROOT;

}
